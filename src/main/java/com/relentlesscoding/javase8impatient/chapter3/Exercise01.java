package com.relentlesscoding.javase8impatient.chapter3;

import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Exercise01 {
    private static Logger logger = Logger.getLogger("logger");

    public static void logIf(final Level logLevel, final Supplier<Boolean> test, final Supplier<String> message) {
        if (logger.isLoggable(logLevel)) {
            if (test.get()) {
                message.get();
            }
        }
    }

}
