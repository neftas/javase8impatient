package com.relentlesscoding.javase8impatient.chapter3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.*;
import java.util.stream.IntStream;

public class Exercise0 {

    public static void printThis(Supplier<String> supplier) {
        System.out.println(supplier.get());
    }

    public static void repeat(int n, IntConsumer action) {
        for (int i = 0; i < n; i++) {
            action.accept(i);
        }
    }

    public static void repeat(int n, Runnable action) {
        for (int i = 0; i < n; i++) {
            new Thread(action).start();
        }
    }

    public static UnaryOperator<String> multiplyStringThisManyTimes(final int times) {
        return x -> {
            final String orig = x;
            for (int i = 0; i < times - 1; i++) {
                x += orig;
            }
            return x;
        };
    }

    public static BiFunction<String, Integer, String> multiplyStringWithThisSeparator(final String sep) {
        return (s, num) -> {
            final StringBuffer sb = new StringBuffer((s.length() + sep.length()) * num - sep.length());
            IntStream.range(0, num).forEach(x -> sb.append(s).append(sep));
            // remove superfluous separator at the end
            sb.delete(sb.length() - sep.length(), sb.length());
            return sb.toString();
        };
    }

    public static <T> UnaryOperator<T> compose(UnaryOperator<T> one, UnaryOperator<T> two) {
        return t -> two.apply(one.apply(t));
    }

    public static class Word {
        private String theWord;
        private List<UnaryOperator<String>> pendingOperations = new ArrayList<>();

        /* avoid direct instantiation */
        private Word(final String theWord) {
            this.theWord = theWord;
        }

        public static Word from(final String theWord) {
            return new Word(theWord);
        }

        public Word transform(final UnaryOperator<String> op) {
            this.pendingOperations.add(op);
            return this;
        }

        @Override
        public String toString() {
            for (UnaryOperator<String> op : pendingOperations) {
                theWord = op.apply(theWord);
            }
            return theWord;
        }
    }

    public static int[][] parallelTransform(int[][] field, UnaryOperator<Integer> f) {
        int numProcessors = Runtime.getRuntime().availableProcessors();
        int height = field.length;
        int width = field[0].length;
        try {
            ExecutorService pool = Executors.newCachedThreadPool();
            for (int i = 0; i < numProcessors; i++) {
                int fromY = i * height / numProcessors;
                int toY = (i + 1) * height / numProcessors;
                pool.submit(() -> {
                    for (int x = 0; x < width; x++) {
                        for (int y = fromY; y < toY; y++) {
                            field[y][x] = f.apply(field[y][x]);
                        }
                    }
                });
            }
            pool.shutdown();
            pool.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        return field;
    }

    public static <T> void doTwoThingsAsync(Supplier<? extends T> supplier, Consumer<? super T> consumer, Consumer<? super Throwable> handler) {
        Thread t = new Thread() {
            public void run() {
                try {
                    T result = supplier.get();
                    consumer.accept(result);
                } catch (Throwable t) {
                    handler.accept(t);
                }
            }
        };
        t.start();
    }

    public static <T> Supplier<T> unchecked(Callable<T> f) {
        return () -> {
            try {
                return f.call();
            } catch (Exception e) {
                throw new RuntimeException(e);
            } catch (Throwable t) {
                throw t;
            }
        };
    }

    public static void readEmployeesAndPersons(List<? extends Person> theList) {
        for (Person person : theList) {
            if (person instanceof Employee) {
                System.out.println("id: " + ((Employee) person).getEmployeeNumber());
            }
        }
    }

    public static void addEmployeeTo(List<? super Employee> persons) {
        persons.add(new Employee("harry", 1));
    }

    public static List<Integer> doAutoboxing() {
        List<Integer> l = new ArrayList<>();
        l.add(1);
        l.add(new Integer(3));
        l.add(new Integer(5));
        l.remove(1);
        l.remove(new Integer(5));

        return l;
    }

    public static void printAnyList(List<?> anyList) {
        for (Object obj : anyList) {
            System.out.println(obj);
        }
    }

    public static long calculateTotal(List<? extends Number> numbers) {
        long total = 0;
        for (Number number : numbers) {
            total = total + number.longValue();
        }
        return total;
    }

    public static void addQuack(List<? super String> l) {
        l.add("Quack");
    }

    <T extends A> T method3(List<T> list) {
        return list.get(0);
    }
}
