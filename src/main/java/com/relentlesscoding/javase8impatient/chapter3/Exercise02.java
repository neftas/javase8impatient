package com.relentlesscoding.javase8impatient.chapter3;

import java.util.concurrent.locks.Lock;

public class Exercise02 {

    public static void withLock(Lock lock, Runnable action) {
        lock.lock();
        try {
            action.run();
        } finally {
            lock.unlock();
        }
    }

}
