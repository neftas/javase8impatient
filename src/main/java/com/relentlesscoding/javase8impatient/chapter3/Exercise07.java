package com.relentlesscoding.javase8impatient.chapter3;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.UnaryOperator;

public class Exercise07 {
    private List<UnaryOperator<String>> pendingAttributes = new ArrayList<>();

    public Exercise07 compose(UnaryOperator<String> operator) {
        pendingAttributes.add(operator);
        return this;
    }

    public Comparator<String> createComparator() {
        Comparator<String> comp = (a, b) -> {
            for (UnaryOperator<String> op : pendingAttributes) {
                a = op.apply(a);
                b = op.apply(b);
            }
            return a.compareTo(b);
        };
        return comp;
    }

}
