package com.relentlesscoding.javase8impatient.chapter3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Employee extends Person {
    private int employeeNumber;

    public Employee(final String name, final int employeeNumber) {
        super(name);
        this.employeeNumber = employeeNumber;
    }

    public static List<Employee> getListContainingThreeEmployees() {
        final List<Employee> employeeList = new ArrayList<>();
        employeeList.add(new Employee("Dennis", 1));
        employeeList.add(new Employee("Sander", 2));
        employeeList.add(new Employee("Jurian", 3));
        return employeeList;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Employee{");
        sb.append("employeeNumber=").append(employeeNumber);
        sb.append('}');
        return sb.toString();
    }

    public int getEmployeeNumber() {
        return employeeNumber;
    }
}
