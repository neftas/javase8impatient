package com.relentlesscoding.javase8impatient.chapter5;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

class Exercise4 {

    static final String monthFormat = "" +
            "Mo Tu We Th Fr Sa Su%n" +
            "%2s %2s %2s %2s %2s %2s %2s%n" +
            "%2s %2s %2s %2s %2s %2s %2s%n" +
            "%2s %2s %2s %2s %2s %2s %2s%n" +
            "%2s %2s %2s %2s %2s %2s %2s%n" +
            "%2s %2s %2s %2s %2s %2s %2s%n" +
            "%2s %2s %2s %2s %2s %2s %2s%n";

    static String cal(final Month month, final int year) {
        String[] args = getDaysOfMonthAsArray(month, year);
        return String.format(monthFormat, args);
    }

    private static String[] getDaysOfMonthAsArray(final Month month, final int year) {
        final LocalDate date = LocalDate.of(year, month, 1);
        final DayOfWeek firstDay = date.getDayOfWeek();
        final int lengthOfMonth = date.getMonth().length(date.isLeapYear());
        final String[] monthArray = new String[6*7];
        final int startPosition = firstDay.getValue() - 1;

        for (int i = 0; i < lengthOfMonth; i++) {
            monthArray[i + startPosition] = String.valueOf(i + 1);
        }

        return Arrays.stream(monthArray).map(Exercise4::replaceNull).toArray(String[]::new);
    }

    private static String replaceNull(final String s) {
        return (s == null) ? "" : s;
    }
}
