package com.relentlesscoding.javase8impatient.chapter5;

import java.time.LocalDate;
import java.time.Month;

public class Exercise1 {

    public static LocalDate getProgrammersDayForYear(final int year) {
        LocalDate date = LocalDate.of(year, Month.JANUARY, 1);
        return date.withDayOfYear(256);
    }
}
