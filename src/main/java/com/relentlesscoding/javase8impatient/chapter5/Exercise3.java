package com.relentlesscoding.javase8impatient.chapter5;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.function.Predicate;

public class Exercise3 {

    public static LocalDate calculateNextWorkDay(final LocalDate date) {
        return date.with(next(w -> w.getDayOfWeek().getValue() < 6));
    }

    private static TemporalAdjuster next(Predicate<LocalDate> predicate) {
        return TemporalAdjusters.ofDateAdjuster(temporal -> {
            LocalDate date = temporal;
            do {
                date = date.plusDays(1);
            } while (!predicate.test(date));
            return date;
        });
    }
}
