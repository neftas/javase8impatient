package com.relentlesscoding.javase8impatient.chapter2;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class Exercise13 {

    public static Map<Integer, Long> countShortWordsOf(final String content) {
        // to filter out words of length 0, use filter(word -> !word.isEmpty())
        return Arrays.stream(content.split("\\P{L}")).parallel()
                .filter(word -> word.length() < 12)
                .collect(Collectors.groupingBy(String::length, Collectors.counting()));
    }
}
