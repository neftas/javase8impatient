package com.relentlesscoding.javase8impatient.chapter2;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class Exercise12 {

    public static AtomicInteger[] countShortWordsOf(final String content) {
        AtomicInteger[] shortWords = new AtomicInteger[12];
        IntStream.range(0, shortWords.length).forEach(x -> shortWords[x] = new AtomicInteger(0));
        Arrays.stream(content.split("\\P{L}")).parallel().forEach(
                word -> {
                    int wordLength = word.length();
                    if (wordLength < 12) {
                        shortWords[wordLength].getAndIncrement();
                    }
                }
        );
        return shortWords;
    }

}
