package com.relentlesscoding.javase8impatient.chapter2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class Exercise11 {

    // most of solution borrowed from https://github.com/galperin/Solutions-for-exercises-from-Java-SE-8-for-the-Really-Impatient-by-Horstmann/blob/master/src/main/java/de/galperin/javase8/capitel2/C2E11.java
    public static <T> List<T> convertStreamToList(T[] array) {
        IntStream intStream = IntStream.range(0, array.length);
        List<T> result = new ArrayList<>(Arrays.asList((T[]) new Object[array.length]));
        intStream.parallel().forEach(x -> result.set(x, array[x]));
        return result;
    }
}
