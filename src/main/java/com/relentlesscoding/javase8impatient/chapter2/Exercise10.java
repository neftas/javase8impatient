package com.relentlesscoding.javase8impatient.chapter2;

import java.util.DoubleSummaryStatistics;
import java.util.OptionalDouble;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Exercise10 {
    public static OptionalDouble getAverageOf(Stream<Double> stream) {
        AtomicInteger count = new AtomicInteger(0);
        double sum = stream.parallel().peek(x -> count.getAndIncrement()).reduce(0d, (total, next) -> total + next );
        return count.get() > 0 ? OptionalDouble.of(sum / count.get()) : OptionalDouble.empty();
    }

    public static OptionalDouble getAverageProperlyOf(Stream<Double> stream) {
        return stream.mapToDouble(x -> x).average();
    }

    public static OptionalDouble getAverageByStatisticsOf(Stream<Double> stream) {
        DoubleSummaryStatistics stats = stream.collect(Collectors.summarizingDouble(Double::doubleValue));
        return stats.getAverage() != 0d ? OptionalDouble.of(stats.getAverage()) : OptionalDouble.empty();
    }
}
