package com.relentlesscoding.javase8impatient.chapter5;

import org.junit.Test;

import java.time.Month;

import static com.relentlesscoding.javase8impatient.chapter5.Exercise4.monthFormat;
import static org.junit.Assert.assertEquals;

public class Exercise4Test {

    @Test
    public void printJanuary2017() {
        final String expected = String.format(monthFormat,
                "", "", "", "", "", "", "1",
                "2", "3", "4", "5", "6", "7", "8",
                "9", "10", "11", "12", "13", "14", "15",
                "16", "17", "18", "19", "20", "21", "22",
                "23", "24", "25", "26", "27", "28", "29",
                "30", "31", "", "", "", "", "");

        final String actual = Exercise4.cal(Month.JANUARY, 2017);

        assertEquals(expected, actual);
    }

    @Test
    public void printJune1900() {
        final String expected = String.format(monthFormat,
                "", "", "", "", "1", "2", "3",
                "4", "5", "6", "7", "8", "9", "10",
                "11", "12", "13", "14", "15", "16", "17",
                "18", "19", "20", "21", "22", "23", "24",
                "25", "26", "27", "28", "29", "30", "",
                "", "", "", "", "", "", "");

        final String actual = Exercise4.cal(Month.JUNE, 1900);

        assertEquals(expected, actual);
    }
}
