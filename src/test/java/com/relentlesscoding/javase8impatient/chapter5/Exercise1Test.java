package com.relentlesscoding.javase8impatient.chapter5;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class Exercise1Test {

    @Test
    public void computeProgrammersDay() {
        LocalDate programmersDay2017 = Exercise1.getProgrammersDayForYear(2017);

        assertEquals("2017-09-13", programmersDay2017.toString());
    }
}
