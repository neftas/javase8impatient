package com.relentlesscoding.javase8impatient.chapter5;

import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.Assert.assertEquals;

public class Exercise2Test {

    @Test
    public void addOneYearTo29thOfFebruari2000() {
        LocalDate feb29th2000 = LocalDate.of(2000, Month.FEBRUARY, 29);
        LocalDate feb29th2000PlusOneYear = feb29th2000.plusYears(1);

        assertEquals("2001-02-28", feb29th2000PlusOneYear.toString());
    }

    @Test
    public void addFourYearTo29thOfFebruari2000() {
        LocalDate feb29th2000 = LocalDate.of(2000, Month.FEBRUARY, 29);
        LocalDate feb29th2000PlusFourYears = feb29th2000.plusYears(4);

        assertEquals("2004-02-29", feb29th2000PlusFourYears.toString());
    }

    @Test
    public void addFourTimesOneYearTo29thOfFebruari2000() {
        LocalDate feb29th2000 = LocalDate.of(2000, Month.FEBRUARY, 29);
        LocalDate feb29th2000PlusFourTimeOneYear = feb29th2000;
        for (int i = 0; i < 4; i++) {
            feb29th2000PlusFourTimeOneYear = feb29th2000PlusFourTimeOneYear.plusYears(1);
        }

        assertEquals("2004-02-28", feb29th2000PlusFourTimeOneYear.toString());
    }
}
