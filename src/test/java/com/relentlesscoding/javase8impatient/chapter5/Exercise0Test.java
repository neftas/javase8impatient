package com.relentlesscoding.javase8impatient.chapter5;

import org.junit.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;

public class Exercise0Test {

    @Test
    public void durationBetweenDates() {
        LocalDate programmersDay = LocalDate.ofYearDay(2017, 256);
        LocalDate birthDay = LocalDate.of(2017, Month.OCTOBER, 20);
        final long expected = 37;

        final long actual = programmersDay.until(birthDay, ChronoUnit.DAYS);

        assertEquals(expected, actual);
    }

    @Test
    public void xmas() {
        MonthDay firstDayOfXmas = MonthDay.of(Month.DECEMBER, 25);
        MonthDay firstOfJanuary = MonthDay.of(Month.JANUARY, 1);
        // cannot calculate period between two MonthDays, because that depends on particular year
    }

    @Test
    public void firstFridayOfMonth() {
        LocalDate firstFridayOctober2017 = LocalDate.of(2017, 10, 1).with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY));
        System.out.println(firstFridayOctober2017);
    }

    @Test
    public void findMaximumFreeDaysWithTemporalAdjusters() {
        TemporalAdjuster weekdayAdjuster = TemporalAdjusters.ofDateAdjuster(d -> {
            LocalDate result = d;
            do {
                result = result.plusYears(1);
            } while (result.getDayOfWeek().getValue() >= 5);
            return result;
        });

        LocalDate firstDayOfXmasOnAWeekDay = LocalDate.of(2017, Month.DECEMBER, 25);
        List<LocalDate> greatXmases = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            firstDayOfXmasOnAWeekDay = firstDayOfXmasOnAWeekDay.with(weekdayAdjuster);
            System.out.format("Next great Xmas: %s (%s)", firstDayOfXmasOnAWeekDay, firstDayOfXmasOnAWeekDay.getDayOfWeek());
            System.out.format(" with New Year's Day on %s (%s)%n", firstDayOfXmasOnAWeekDay.plusWeeks(1), firstDayOfXmasOnAWeekDay.plusWeeks(1).getDayOfWeek());
        }
    }

    @Test
    public void sleepFor8StraightHours() {
        LocalTime bedTime = LocalTime.of(22, 00);
        LocalTime wakeupTime = bedTime.plusHours(8);

        assertThat(wakeupTime.getHour(), is(equalTo(6)));
    }

    @Test
    public void zonedTime() {
        System.out.println(ZoneId.getAvailableZoneIds().contains("Europe/Amsterdam"));
        ZoneId newYorkZoneId = ZoneId.of("America/New_York");
        ZoneId amsterdamZoneId = ZoneId.of("Europe/Amsterdam");
        ZonedDateTime apollo11LaunchTimeInNewYork = ZonedDateTime.of(1969, 7, 16, 9, 32, 0, 0, newYorkZoneId);
        ZonedDateTime apollo11LaunchTimeInAmsterdam = apollo11LaunchTimeInNewYork.withZoneSameInstant(amsterdamZoneId);
        System.out.println(apollo11LaunchTimeInAmsterdam);
    }

    @Test
    public void formatBasicISODate() {
        LocalDate date = LocalDate.of(2017, Month.NOVEMBER, 22);
        DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;
        assertEquals("20171122", formatter.format(date));
    }

    @Test
    public void formatISOLocalTime() {
        LocalTime time = LocalTime.of(23, 30);
        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_TIME;
        assertEquals("23:30:00", formatter.format(time));
    }

    @Test
    public void formatISOOffsetDateTime() {
        LocalDateTime dateTime = LocalDateTime.of(2017, Month.NOVEMBER, 22, 9, 9);
        ZonedDateTime zonedDateTime = dateTime.atZone(ZoneId.of("Europe/Amsterdam"));
        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

        assertEquals("2017-11-22T09:09:00+01:00", formatter.format(zonedDateTime));
    }

    @Test
    public void formatISOInstant() {
        LocalDateTime dateTime = LocalDateTime.of(2017, Month.NOVEMBER, 22, 9, 17);
        ZonedDateTime zonedDateTime = dateTime.atZone(ZoneOffset.UTC);
        Instant now = Instant.from(zonedDateTime);
        DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;

        assertEquals("2017-11-22T09:17:00Z", formatter.format(now));
    }

    @Test
    public void formatISOOrdinalDate() {
        LocalDate localDate = LocalDate.of(2017, Month.FEBRUARY, 1);
        DateTimeFormatter formatter = DateTimeFormatter.ISO_ORDINAL_DATE;

        assertEquals("2017-032", formatter.format(localDate));
    }

    @Test
    public void formatISOWeekDate() {
        LocalDate localDate = LocalDate.of(2017, Month.NOVEMBER, 22);
        DateTimeFormatter formatter = DateTimeFormatter.ISO_WEEK_DATE;

        assertEquals("2017-W47-3", formatter.format(localDate));
    }

    @Test
    public void formatRFC1123DateTime() {
        LocalDate date = LocalDate.of(2017, Month.NOVEMBER, 22);
        LocalTime time = LocalTime.of(9, 25);
        ZonedDateTime localDateTime = ZonedDateTime.of(date, time, ZoneId.of("Europe/Amsterdam"));
        DateTimeFormatter formatter = DateTimeFormatter.RFC_1123_DATE_TIME;

        assertEquals("Wed, 22 Nov 2017 09:25:00 +0100", formatter.format(localDateTime));
    }

    @Test
    public void formatDateWithDutchLocale() {
        LocalDate date = LocalDate.of(2017, Month.NOVEMBER, 22);
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG);
        DateTimeFormatter dutchFormatter = formatter.withLocale(new Locale("nl"));

        assertEquals("22 november 2017", dutchFormatter.format(date));
    }

    @Test
    public void parseLocalDate() {
        LocalDate date = LocalDate.parse("2017-11-22");

        assertEquals(date.getYear(), 2017);
        assertEquals(date.getMonth(), Month.NOVEMBER);
        assertEquals(date.getDayOfWeek(), DayOfWeek.WEDNESDAY);
    }

    @Test
    public void parseApollo11LaunchTimeWithCustomParser() {
        ZonedDateTime apollo11Launch = ZonedDateTime.parse("1969-07-16 03:32:00-0400",
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ssxx"));

        assertThat(apollo11Launch.getYear(), is(1969));
        assertThat(apollo11Launch.getMonth(), is(Month.JULY));
        assertThat(apollo11Launch.getDayOfMonth(), is(16));
        assertThat(apollo11Launch.getHour(), is(3));
        assertThat(apollo11Launch.getMinute(), is(32));
        assertThat(apollo11Launch.getSecond(), is(00));
        assertThat(apollo11Launch.getOffset(), is(ZoneOffset.of("-4")));
    }

    @Test
    public void formatDateTimeAsISO8601() {
        LocalDate birthday = LocalDate.of(1994, Month.JUNE, 6);
        final String formattedDate = DateTimeFormatter.ISO_DATE.format(birthday);
        assertEquals("1994-06-06", formattedDate);
    }

    @Test
    public void withLocale() {
        LocalDate birthday = LocalDate.of(1977, 12, 28);
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG).withLocale(new Locale("nl"));
        assertEquals("28 december 1977", formatter.format(birthday));
    }
}
