package com.relentlesscoding.javase8impatient.chapter5;

import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.TemporalAdjusters;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Exercise3Test {

    @Test
    public void computeNextWorkdayWhenDayIsWednesday() {
        LocalDate nov22nd2017 = LocalDate.of(2017, Month.NOVEMBER, 22);
        assertThat(nov22nd2017.getDayOfWeek(), is(DayOfWeek.WEDNESDAY));

        LocalDate nextWorkDay = Exercise3.calculateNextWorkDay(nov22nd2017);

        assertThat(nextWorkDay.toString(), is("2017-11-23"));
    }

    @Test
    public void computeNextWorkDayWhenDayIsFriday() {
        LocalDate firstFridayInNovember2017 = LocalDate.of(2017, Month.NOVEMBER, 1);
        firstFridayInNovember2017 = firstFridayInNovember2017.with(TemporalAdjusters.firstInMonth(DayOfWeek.FRIDAY));
        assertThat(firstFridayInNovember2017.getDayOfWeek(), is(DayOfWeek.FRIDAY));

        LocalDate nextWorkDay = Exercise3.calculateNextWorkDay(firstFridayInNovember2017);

        assertThat(nextWorkDay.getDayOfWeek(), is(DayOfWeek.MONDAY));
    }
}
