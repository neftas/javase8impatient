package com.relentlesscoding.javase8impatient.chapter2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.OptionalDouble;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Exercise10Test {
    private Stream<Double> stream;
    private OptionalDouble optAvg;

    @Before
    public void setup() {
    }

    @After
    public void tearDown() {
        stream = null;
        optAvg = null;
    }

    public void whenCalculatingAverageOfStream() {
        optAvg = Exercise10.getAverageOf(stream);
    }

    public void whenCalculatingAverageOfStreamProperly() {
        optAvg = Exercise10.getAverageProperlyOf(stream);
    }

    public void whenCalculatingAverageOfStreamByUsingStatistics() {
        optAvg = Exercise10.getAverageByStatisticsOf(stream);
    }

    public void givenEmptyStream() {
        stream = Stream.empty();
    }

    public void givenStreamOf(double... nums) {
        stream = Arrays.stream(nums).boxed();
    }

    public void thenWeDoNotHaveAnAverage() {
        assertFalse(optAvg.isPresent());
    }

    public void thenWeHaveAnAverage() {
        assertTrue(optAvg.isPresent());
    }

    public void thenAverageIsEqualTo(final double expected) {
        assertEquals(expected, optAvg.getAsDouble(), 0.01);
    }

    @Test
    public void averageStreamShouldReturnEmptyOptionalWhenInputIsEmptyStream() {
        givenEmptyStream();

        whenCalculatingAverageOfStream();

        thenWeDoNotHaveAnAverage();
    }

    @Test
    public void averageStreamShouldReturnAverage() {
        // arrange or build
        givenStreamOf(1d, 3d, 5d, 7d);

        // act or operate
        whenCalculatingAverageOfStream();

        // assert or check
        thenWeHaveAnAverage();
        thenAverageIsEqualTo(4d);
    }

    @Test
    public void averageStreamShouldReturnSameNumberWhenInputIsStreamWithSingleInput() {
        givenStreamOf(1d);

        whenCalculatingAverageOfStream();

        thenWeHaveAnAverage();
        thenAverageIsEqualTo(1d);
    }

    @Test
    public void averageStreamProperlyShouldReturnEmptyOptionalWhenStreamIsEmpty() {
        givenEmptyStream();

        whenCalculatingAverageOfStreamProperly();

        thenWeDoNotHaveAnAverage();
    }

    @Test
    public void averageStreamProperlyShouldReturnSameNumberAsStreamWithSingleEntry() {
        givenStreamOf(100d);

        whenCalculatingAverageOfStreamProperly();

        thenWeHaveAnAverage();
        thenAverageIsEqualTo(100d);
    }

    @Test
    public void averageStreamProperlyShouldReturnCorrectOverMultipleNumbers() {
        givenStreamOf(10, 20d, 30d, 40d, 50d, 60d, 70d);

        whenCalculatingAverageOfStreamProperly();

        thenWeHaveAnAverage();
        thenAverageIsEqualTo(40d);
    }

    @Test
    public void averageStreamProperlyWithStatisticsEmptyStream() {
        givenEmptyStream();

        whenCalculatingAverageOfStreamByUsingStatistics();

        thenWeDoNotHaveAnAverage();
    }

    @Test
    public void averageStreamProperlyWithStatisticsSingleElement() {
        givenStreamOf(1d);

        whenCalculatingAverageOfStreamByUsingStatistics();

        thenWeHaveAnAverage();
        thenAverageIsEqualTo(1d);
    }

    @Test
    public void averageStreamProperlyWithStatisticsMultipleElements() {
        givenStreamOf(1d, 2d, 3d);

        whenCalculatingAverageOfStreamByUsingStatistics();

        thenWeHaveAnAverage();
        thenAverageIsEqualTo(2d);
    }
}
