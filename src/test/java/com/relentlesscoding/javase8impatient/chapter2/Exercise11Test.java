package com.relentlesscoding.javase8impatient.chapter2;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class Exercise11Test {
    Object[] array;
    List<Object> result;

    public void thenListContainsAllElementsFromArray() {
        assertThat(result, containsInAnyOrder(array));
    }

    public void givenThisArray(Object... items) {
        array = items;
    }

    public void whenConvertingArrayToList() {
        result = Exercise11.convertStreamToList(array);
    }

    public void thenArrayLengthAndListLengthAreTheSame() {
        assertTrue(array.length == result.size());
    }

    @Test
    public void testWithSmallArray() {
        givenThisArray(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        whenConvertingArrayToList();

        thenArrayLengthAndListLengthAreTheSame();
        thenListContainsAllElementsFromArray();
    }

    @Test
    public void testWithLargerArray() {
        Character[] alphabet = Stream.iterate('A', c -> (char) (c + 1)).limit(50).toArray(Character[]::new);
        givenThisArray((Object[]) alphabet);

        whenConvertingArrayToList();

        thenArrayLengthAndListLengthAreTheSame();
        thenListContainsAllElementsFromArray();
    }

    @Test
    public void testWithHugeArray() {
        Integer[] ints = Stream.iterate(0, n -> n + 1).limit(10_000).toArray(Integer[]::new);
        givenThisArray((Object) ints);

        whenConvertingArrayToList();

        thenArrayLengthAndListLengthAreTheSame();
        thenListContainsAllElementsFromArray();
    }
}
