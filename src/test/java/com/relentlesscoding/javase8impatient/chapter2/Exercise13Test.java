package com.relentlesscoding.javase8impatient.chapter2;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertTrue;

public class Exercise13Test {
    private static String content;

    @BeforeClass
    public static void setupClass() throws URISyntaxException, IOException {
        final Path path = Paths.get(Exercise13Test.class.getClassLoader().getResource("war_and_peace.txt").toURI());
        content = new String(Files.readAllBytes(path));
    }

    @Test
    public void exercise12And13ShouldReturnSameResults() {
        Map<Integer, Long> ex13result = Exercise13.countShortWordsOf(content);
        AtomicInteger[] ex12Array = Exercise12.countShortWordsOf(content);
        for (int i = 0, j = ex12Array.length; i < j; i++) {
            assertTrue(ex12Array[i].get() == ex13result.get(i));
        }
    }
}
