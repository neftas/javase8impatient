package com.relentlesscoding.javase8impatient.chapter2;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

public class Exercise12Test {
    private static String content;
    private AtomicInteger[] result;

    @BeforeClass
    public static void setupClass() throws IOException, URISyntaxException {
        final Path path = Paths.get(Exercise12Test.class.getClassLoader().getResource("war_and_peace.txt").toURI());
        content = new String(Files.readAllBytes(path));
    }

    @Test
    public void countShortWordsShouldReturnAFilledArray() {
        whenCountingSmallWordsInWarAndPeace();

        thenArrayIsFilled();
    }

    private void thenArrayIsFilled() {
        assertTrue(result.length > 0);
    }

    private void thenArraysAreEqual(final AtomicInteger[] first, final AtomicInteger[] second) {
        final int lengthFirst = first.length;
        final int lengthSecond = second.length;
        if (lengthFirst != lengthSecond) {
            fail(String.format("arrays are not of equal length: '%d' and '%d'", lengthFirst, lengthSecond));
        }
        for (int i = 0; i < lengthFirst; i++) {
            assertEquals(first[i].get(), second[i].get());
        }
    }

    @Test
    public void countShortWordsShouldReturnSameResultEveryTime() throws URISyntaxException, IOException {
        // to see if different invocations result in different numbers (and thus in concurrency problems)
        // we invoke the method under test 10 times and compare the results
        AtomicInteger[] firstInvocationResult = Exercise12.countShortWordsOf(content);
        for (int i = 0; i < 10; i++) {
            whenCountingSmallWordsInWarAndPeace();
            thenArraysAreEqual(firstInvocationResult, result);
        }
    }

    private void whenCountingSmallWordsInWarAndPeace() {
        result = Exercise12.countShortWordsOf(content);
    }
}
