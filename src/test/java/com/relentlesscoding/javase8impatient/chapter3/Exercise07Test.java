package com.relentlesscoding.javase8impatient.chapter3;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Comparator;
import java.util.function.UnaryOperator;

import static org.junit.Assert.assertTrue;

public class Exercise07Test {
    private Exercise07 stringComparatorBuilder;
    private Comparator<String> comparator;

    @Before
    public void setup() {
        stringComparatorBuilder = new Exercise07();
    }

    @After
    public void tearDown() {
        stringComparatorBuilder = null;
    }

    private void givenComparatorWithTheseAttributes(UnaryOperator<String>... ops) {
        for (UnaryOperator<String> op : ops) {
            stringComparatorBuilder.compose(op);
        }
        comparator = stringComparatorBuilder.createComparator();
    }

    private void givenComparatorWithNoCustomAttributes() {
        comparator = stringComparatorBuilder.createComparator();
    }

    private void thenTheseStringsAreNotConsideredEqual(final String a, final String b) {
        assertTrue(comparator.compare(a, b) != 0);
    }

    private void thenTheseStringsAreConsideredEqual(final String a, final String b) {
        assertTrue(comparator.compare(a, b) == 0);
    }

    @Test
    public void withTrimmingAndUpperCasing() {
        givenComparatorWithTheseAttributes(String::toUpperCase, String::trim);

        thenTheseStringsAreConsideredEqual(" a ", "A");
    }

    @Test
    public void noAttributes() {
        givenComparatorWithNoCustomAttributes();

        thenTheseStringsAreNotConsideredEqual(" a ", "A");
    }

    @Test
    public void reversed() {
        givenComparatorWithNoCustomAttributes();

        assertTrue(false);
    }
}
