package com.relentlesscoding.javase8impatient.chapter3;

import org.junit.Test;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;

public class Exercise01Test {

    @Test
    public void testLazyLogging() {
        final int i = 10;
        final int[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        Exercise01.logIf(Level.FINEST, () -> i == 10, () -> "a[10] = " + a[10]);
    }

}
