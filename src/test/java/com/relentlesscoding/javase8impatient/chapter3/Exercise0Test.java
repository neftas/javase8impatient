package com.relentlesscoding.javase8impatient.chapter3;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class Exercise0Test {

    @Test
    public void testOwnStringSupplier() {
        Exercise0.printThis(() -> "this was a triumph");
    }

    @Test
    public void testIntConsumer() {
        Exercise0.repeat(10, i -> System.out.println("Countdown: " + (9 - i)));
    }

    @Test
    public void testRunnableConsumer() {
        Exercise0.repeat(5, () -> System.out.println(Thread.currentThread().getName()));
    }

    @Test
    public void multiplyAString10Times() {
        final UnaryOperator<String> op = Exercise0.multiplyStringThisManyTimes(10);
        final String result = op.apply("bla");
        assertEquals("blablablablablablablablablabla", result);
    }

    @Test
    public void multiplyAStringArbitraryNumberOfTimes() {
        final BiFunction<String, Integer, String> func = Exercise0.multiplyStringWithThisSeparator(", ");
        final String result = func.apply("a", 5);
        assertEquals("a, a, a, a, a", result);
    }

    @Test
    public void oneAfterTheOther() {
        final String s = " bla ";
        UnaryOperator<String> op = Exercise0.compose(String::toUpperCase, String::trim);
        final String actual = op.apply(s);
        assertEquals("BLA", actual);
    }

    @Test
    public void testLazyEvaluation() {
        final String s = " bla ";
        final String actual = Exercise0.Word.from(s).transform(String::toUpperCase).transform(String::trim).toString();
        assertEquals("BLA", actual);
    }

    @Test
   public void testParallelExecution() {
        int[][] field = new int[8][8];
        /**
         *  1 |  2 |  3 |  4 |  5 |  6 |  7 |  8
         *  9 | 10 | 11 | 12 | 13 | 14 | 15 | 16
         * 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24
         * ... snip ...
         * 57 | 58 | 59 | 60 | 61 | 62 | 63 | 64
         */
        for (int y = 0; y < field.length; y++) {
            for (int x = 0; x < field[0].length; x++) {
                field[y][x] = (y * field.length) + x + 1;
            }
        }
        print2DArray(field);
        UnaryOperator<Integer> squareIt = x -> x * x;
        int[][] actual = Exercise0.parallelTransform(field, squareIt);
        int[][] expected = new int[8][8];
        for (int y = 0; y < expected.length; y++) {
            for (int x = 0; x < expected[0].length; x++) {
                int value = (y * expected.length) + x + 1;
                expected[y][x] = value * value;
            }
        }
        print2DArray(actual);
        assertArrayEquals(expected, actual);
    }

    public static void print2DArray(int[][] array) {
        for (int y = 0; y < array.length; y++) {
            for (int x = 0; x < array[0].length; x++) {
                System.out.print(array[y][x] + " ");
            }
            System.out.println();
        }
    }

    @Test
    public void doTwoThingsThrowsException() {
        Supplier<String> supplier = () -> { throw new RuntimeException("oh no!"); };
        Consumer<String> consumer = System.out::println;
        Consumer<Throwable> handler = (t) -> System.out.println("no big deal, whatever");
        Exercise0.doTwoThingsAsync(supplier, consumer, handler);
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void checkedExceptionCallable() {
        Supplier<String> supplier = Exercise0.unchecked(() -> new String(Files.readAllBytes(Paths.get("/etc/passwd")), StandardCharsets.UTF_8));
        System.out.println(supplier.get());
    }

    @Test
    public void listPersonsAndEmployees() {
        List<Employee> employeeList = Employee.getListContainingThreeEmployees();
        Exercise0.readEmployeesAndPersons(employeeList);
    }

    @Test
    public void addEmployeeToSuperEmployee() {
        List<Employee> employees = Employee.getListContainingThreeEmployees();
        Exercise0.addEmployeeTo(employees);
    }

    @Test
    public void testOverloadedRemoveMethodOfList() {
        List<Integer> result = Exercise0.doAutoboxing();
        assertEquals(1, result.size());
        assertEquals(1, (int) result.get(0));
    }

    @Test
    public void printAnyList() {
        List<String> numbers = new ArrayList<>(Arrays.asList("one", "two"));
        Exercise0.printAnyList(numbers);
        List<Employee> employees = Employee.getListContainingThreeEmployees();
        Exercise0.printAnyList(employees);
    }

    @Test
    public void calculateTotalWithAllKindsOfNumbers() {
        List<Double> doubles = new ArrayList<>();
        doubles.add(2.1d);
        doubles.add(5.7d);
        doubles.add(12.4d);
        long actual = Exercise0.calculateTotal(doubles);
        assertEquals(19L, actual);
    }

    @Test
    public void addStringToAnyList() {
        List<String> strings = new ArrayList<>(Arrays.asList("a"));
        List<Object> objects = new ArrayList<>(strings);
        Exercise0.addQuack(strings);
        System.out.println(strings);
        System.out.println(objects);
        Exercise0.addQuack(objects);
        System.out.println(objects);
    }

    @Test
    public void testSuperTypes() {
        List<? super IOException> exceptions = new ArrayList<>();
        // exceptions.add(new Exception()); // won't compile
        exceptions.add(new IOException());
        exceptions.add(new FileNotFoundException());
    }

    @Test
    public void testLowerBound() {
        List<?> exceptions = new ArrayList<Object>();  // read-only
        Object obj = new String("hello");
    }

    @Test
    public void testGenericParameters() {
    }
}
