package com.relentlesscoding.javase8impatient.chapter3;

import org.junit.Test;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Exercise02Test {

    @Test
    public void testReentrantLock() throws InterruptedException {
        Lock myLock = new ReentrantLock();
        for (int i = 0; i < 5; i++) {
            Exercise02.withLock(myLock, () -> System.out.println("Hello, World!"));
            Exercise02.withLock(myLock, () -> System.out.println("Goodbye, World!"));
        }
    }
}
