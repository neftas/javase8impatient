# Solutions to Java SE 8 for the Really Impatient

This repository contains some of the solutions to the exercises in the book
"Java SE 8 for the Really Impatient" by Cay S. Horstmann.
